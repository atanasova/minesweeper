/**
 * Created by atanasova on 10.11.2016.
 */
public class Feld {
    private boolean flagge;
    private boolean bombe;
    private boolean aufgedeckt;
    private int nachbarn;
    private int x;
    private int y;
    //public int anzahlBomben;

    public Feld(int x, int y, boolean flagge, boolean bombe, boolean aufgedeckt, int nachbarn) {
        this.x = x;
        this.y = y;
        this.flagge = flagge;
        this.bombe = bombe;
        this.aufgedeckt = aufgedeckt;
        this.nachbarn = nachbarn;
    }

    public int getX() {
        return this.x;
    }
    public int getY(){
        return this.y;
    }

    public void setAnzahlBomben(int anzahlBomben) {
        this.nachbarn = anzahlBomben;
    }

    public boolean isFlagge() {
        return flagge;
    }

    public void setFlagge(boolean flagge) {
        this.flagge = flagge;
    }

    public boolean isBombe() {
        return bombe;
    }

    public void setBombe(boolean bombe) {
        this.bombe = bombe;
    }

    public boolean isAufgedeckt() {
        return aufgedeckt;
    }

    public void setAufgedeckt(boolean aufgedeckt) {
        this.aufgedeckt = aufgedeckt;
    }

    public int getNachbarn() {
        return nachbarn;
    }

    public void setNachbarn(int nachbarn) {
        this.nachbarn = nachbarn;
    }
}
