import java.util.Observable;
import java.util.Observer;

public class Controller implements Observer {

  private Model model;
  private View view;

  public Controller(Model model, View view) {
    this.model = model;
    this.view = view;

    // wichtig, da sonst das Modell nicht weiss, dass es dieses Objekt bei Aenderungen benachritigen soll.
    model.addObserver(this);

    for (int x = 0; x < Model.BREITE; x++) {
      for (int y = 0; y < Model.HOEHE; y++) {
        int finalX = x;
        int finalY = y;
        view.getLightButton(x, y).setOnMousePressed(
                event -> {
                  if (event.isPrimaryButtonDown())
                    model.aufdecken(finalX, finalY);
                  if (event.isSecondaryButtonDown())
                    model.flaggesetzen(finalX, finalY);
                });
      }
    }

    view.getStage().show();
  }


  /**
   * Wird bei jeder Aenderung am Modell aufgerufen.
   */
  @Override
  public void update(Observable o, Object arg){
  }
}