import java.util.Observable;

public class Model extends Observable {


  public static int BREITE = 9; //wieviele Felder
  public static int HOEHE = 9;  //wieviele Felder

  // Zustaende der Felder
  private final Feld[][] minenfeld;
  private boolean verloren;
  private int bombenzahl;
  private boolean spielende;

  public Model() {
    minenfeld = new Feld[BREITE][HOEHE];
    for (int x = 0; x < BREITE; x++){
      for (int y = 0; y < HOEHE; y++){
        minenfeld[x][y]= new Feld(x,y,false,false,false,0);
      }
    }
    randomizeMines();
    spielende = false;
  }

  /**
   * Setzt die Bombe auf zufaellige Felder auf.
   */
  public void randomizeMines() {
    for (int i = 0; i < BREITE; i++) {
      for (int y = 0; y < HOEHE; y++) {
        boolean bombe = (Math.random() > 0.90);
        minenfeld[i][y] = new Feld(i, y, false, bombe, false, 0);

      }

      setChanged();
      notifyObservers();
    }
  }


  public Feld getFeld(int x,int y) {
    return minenfeld[x][y];
  }


  public void Bombeaufdecken(int x, int y) {

    Feld feld = minenfeld[x][y];


    if (!minenfeld[x][y].isAufgedeckt()) {
      minenfeld[x][y].setAufgedeckt(true);
      if (minenfeld[x][y].isBombe()) {
        verloren=true;
        for(int i=0; i<BREITE; i++){
          for(int j=0; j<HOEHE; j++){
            if(minenfeld[x][y].isBombe() && !minenfeld[x][y].isFlagge()){
              minenfeld[x][y].setAufgedeckt(true);
            }
          }
        }

      }
    }
  }

  public void Zahlenaufdecken(int x, int y){

    int nachbarn=0;

    //rechts
    if((x+1)<BREITE && y>0 && y<HOEHE){
      Feld neigborfeld = minenfeld[x+1][y];
      if (neigborfeld.isBombe()){
        nachbarn++;
      }
    }

    //links
    if((x-1)>=0){
      Feld neigborfeld = minenfeld[x-1][y];
      if (neigborfeld.isBombe()){
        //System.out.println("fehler");
        nachbarn++;
      }
    }

    //unten mitte
    if((y+1)<BREITE){
      Feld neigborfeld = minenfeld[x][y+1];
      if (neigborfeld.isBombe()){
        nachbarn++;
      }
    }

    //oben mitte
    if( (y-1)>=0){
      Feld neigborfeld = minenfeld[x][y-1];
      if (neigborfeld.isBombe()){
        nachbarn++;
      }
    }

    //unten rechts
    if((x+1)<BREITE && (y+1)<HOEHE){
      Feld neigborfeld = minenfeld[x+1][y+1];
      if (neigborfeld.isBombe()){
        nachbarn++;
      }
    }

    //oben rechts
    if((x+1)<BREITE && (y-1)>=0 ){
      Feld neigborfeld = minenfeld[x+1][y-1];
      if (neigborfeld.isBombe()){
        nachbarn++;
      }
    }

    //unten links
    if((x-1)>=0 && (y+1)<BREITE){
      Feld neigborfeld = minenfeld[x-1][y+1];
      if (neigborfeld.isBombe()){
        nachbarn++;
      }
    }

    //oben links
    if((x-1)>=0 && (y-1)>=0){
      Feld neigborfeld = minenfeld[x-1][y-1];
      if (neigborfeld.isBombe()){
        nachbarn++;
      }
    }

    //if nachbarn =0;
   //wenn nachbarn 0, dann alle aufdecken

    bombenzahl = nachbarn;
    minenfeld[x][y].setAnzahlBomben(nachbarn);

    minenfeld[x][y].setAufgedeckt(true);
    setChanged();
    notifyObservers();
  }


  public void Leeretaste (int x, int y) {

    if (minenfeld[x][y].getNachbarn() == 0) {

      if ((x + 1) < BREITE && y < HOEHE) {
        aufdecken(x + 1, y);
      }
      if (((x - 1) < BREITE) && y < HOEHE) {
        aufdecken(x - 1, y);
      }
      if (x < BREITE && (y + 1) < HOEHE) {
        aufdecken(x, y + 1);
      }
      if (x < BREITE && (y - 1) < HOEHE) {
        aufdecken(x, y - 1);
      }
      if ((x + 1) < BREITE && (y + 1) < HOEHE) {
        aufdecken(x + 1, y + 1);
      }
      if ((x - 1) < BREITE && (y + 1) < HOEHE) {
        aufdecken(x - 1, y + 1);
      }
      if ((x + 1) < BREITE && (y - 1) < HOEHE) {
        aufdecken(x + 1, y - 1);
      }
      if ((x - 1) < BREITE && (y - 1) < HOEHE) {
        aufdecken(x - 1, y - 1);
      }


      minenfeld[x][y].setAufgedeckt(true);


      setChanged();
      notifyObservers();

    }
  }
 public void aufdecken(int x, int y) {
   //if (minenfeld[x][y] == null) return;
   if (!minenfeld[x][y].isFlagge()){
     if (minenfeld[x][y].isBombe()) {
       Bombeaufdecken(x, y);
     } else {
       Zahlenaufdecken(x, y);
     }
   }

   setChanged();
   notifyObservers();
 }






  /**
   * Gibt true zurueck, wenn keine Bombe und alle felder ohne Minen aufgedeckt.
   */
  public boolean spielendepruefen() {
    for (int i = 0; i < BREITE; i++) {
      for (int y = 0; y < HOEHE; y++) {
        Feld feld = minenfeld[i][y];
        if (feld.isBombe()&& feld.isAufgedeckt()) {
          return true;
         // if (!feld.isFlagge()) return false;
         // if (feld.isAufgedeckt()) return true;{
        } else {
          if (!feld.isAufgedeckt()) return false;
        }
      }
    }
    return true;
  }

  public void flaggesetzen(int x, int y) {
    Feld feld = minenfeld[x][y];
    boolean flagge = feld.isFlagge();
    feld.setFlagge(!flagge);
    setChanged();
    notifyObservers();
  }
}