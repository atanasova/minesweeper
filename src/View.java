import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.*;
import javafx.scene.text.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;


import java.util.Observable;
import java.util.Observer;

public class View implements Observer {

  private Stage stage;
  private Button[][] minenButtons;
  private Group root;



  private Model model;

  public View(Model model, Stage stage) {
    this.model = model;
    this.stage = stage;
    stage.setTitle("Minesweeper");

    // wichtig, da sonst das Modell nicht weiss, dass es dieses Objekt bei Aenderungen benachritigen soll.
    model.addObserver(this);
    root = new Group();


    GridPane gridpane= new GridPane();
    minenButtons = new Button[Model.BREITE][Model.HOEHE];
    for (int i = 0; i < Model.BREITE; i++) {
      for (int y = 0; y < Model.HOEHE; y++) {
        Button button = new Button(" ");
        button.setPrefWidth(50);
        button.setPrefHeight(50);
        gridpane.add(button, i, y);
        minenButtons[i][y] = button;
      }
    }

    Scene scene = new Scene(root);
    root.getChildren().add(gridpane);
    getStage().setScene(scene);
    stage.show();

    // Aufruf der Methode, die bei jeder Aenderung am Modell aufgerufen wird, damit die richtigen
    // Daten eingetragen werden.
    update(model, null);
  }

  /**
   * Wird bei jeder Aenderung am Modell aufgerufen.
   */
  @Override
  public void update(Observable o, Object arg) {
    for (int i = 0; i < Model.BREITE; i++) {
      for (int y = 0; y < Model.HOEHE; y++) {
        final Feld feld = model.getFeld(i, y);
        if (feld.isAufgedeckt()) {
          // when bombe dann X malen
          if (feld.isBombe()) {
            minenButtons[i][y].setText("X");
          } else {
            int nachbarn = feld.getNachbarn();
            minenButtons[i][y].setText(nachbarn > 0 ? feld.getNachbarn() + "" : " ");
            minenButtons[i][y].setDisable(true);
          }
        } else {
          minenButtons[i][y].setDisable(false);
          minenButtons[i][y].setText(feld.isFlagge() ? "F" : " ");
        }
      }
    }
  }

  public Stage getStage() {
    return stage;
  }


  public Button getLightButton(int x, int y) {
    return minenButtons[x][y];
  }


}
